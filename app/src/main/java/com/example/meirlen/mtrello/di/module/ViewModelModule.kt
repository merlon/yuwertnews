/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.meirlen.mtrello.di.module


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.meirlen.mtrello.di.qualifires.ViewModelKey
import com.example.meirlen.mtrello.viewmodel.BoardsViewModel
import com.example.meirlen.mtrello.viewmodel.ColumsViewModel


import com.example.meirlen.mtrello.viewmodel.TrelloViewModelFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(BoardsViewModel::class)
    abstract fun bindBoardsViewModel(bViewModel: BoardsViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ColumsViewModel::class)
    abstract fun bindColumnsViewModel(cViewModel: ColumsViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(factory: TrelloViewModelFactory): ViewModelProvider.Factory
}
