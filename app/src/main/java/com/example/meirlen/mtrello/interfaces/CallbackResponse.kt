package com.example.meirlen.mtrello.interfaces

import com.example.meirlen.mtrello.vo.Resource

interface CallbackResponse<in T> {
    fun onResponse(response : Resource<T>)
}