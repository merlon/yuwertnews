package com.example.meirlen.mtrello.koin

import androidx.lifecycle.MutableLiveData
import com.example.meirlen.mtrello.ext.with
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.koin.repository.WeatherRepository
import com.example.meirlen.mtrello.util.rx.SchedulerProvider


/**
 * Weather Presenter
 */
class DetailViewModel(private val weatherRepository: WeatherRepository, private val scheduler: SchedulerProvider) : AbstractViewModel() {

    val uiData = MutableLiveData<List<Board>>()

    fun getDetail() {
        launch {
            weatherRepository.getWeather().with(scheduler)
                    .subscribe(
                            { d -> uiData.value = d },
                            { e -> println("got error : $e") })
        }
    }
}