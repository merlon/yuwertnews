package com.example.meirlen.mtrello.koin

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.base.BaseKoinFragment
import com.example.meirlen.mtrello.ext.toast
import com.example.meirlen.mtrello.interfaces.ItemClickListener
import com.example.meirlen.mtrello.model.News
import com.example.meirlen.mtrello.model.Error
import com.example.meirlen.mtrello.ui.adapter.BoardsAdapter
import com.example.meirlen.mtrello.ui.adapter.ResultAdapter
import com.example.meirlen.mtrello.vo.Resource
import kotlinx.android.synthetic.main.board_list_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResultListFragment : BaseKoinFragment<List<News>>(), ItemClickListener<News> {


    val model: DetailViewModel by viewModel()
    private lateinit var mAdapter: ResultAdapter




    override fun getContentView(): Int {
        return R.layout.board_list_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = ResultAdapter(context!!,this)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
        model.getDetail()
      /*  model.uiData.observe(this, Observer<List<News>> {
            if (it != null) {
                mAdapter.setData(it as ArrayList<News>)

            }
        })*/

        val list: MutableList<News> = mutableListOf(News("Хозяйка массажного салона, который был признан борделем, осуждена к условному сроку за содержание притона. Женщина не смогла убедить Верховный суд в необходимости отмены приговора, передает корреспондент","Верховный суд признал массажный салон борделем в Караганде",1,"https://netstorage-nur.akamaized.net/images/65c72896bbcfa708.jpg", emptyList()),
                News("Полиция задержала мужчину, заявившего, что Санта-Клауса не существует","Полиция задержала мужчину, заявившего, что Санта-Клауса не существует",1,"https://netstorage-nur.akamaized.net/images/98cc836432f9ad3e.jpg", emptyList()),News("Хозяйка массажного салона, который был признан борделем, осуждена к условному сроку за содержание притона. Женщина не смогла убедить Верховный суд в необходимости отмены приговора, передает корреспондент","Верховный суд признал массажный салон борделем в Караганде",1,"https://netstorage-nur.akamaized.net/images/65c72896bbcfa708.jpg", emptyList()))
        mAdapter.setData(list as ArrayList<News>)
        
    }

    override fun onResponse(response: Resource<List<News>>) {
        if (response.data != null && response.data.isNotEmpty()) {
            mAdapter.setData(response.data as ArrayList<News>)
            showContent(response.data)
        } else {
            val error = Error.NonCritical(resources.getString(R.string.error_no_results))
            showError(error)
        }
    }

    override fun onItemClick(dataObject: News) {
        //context.toast()
    }


}