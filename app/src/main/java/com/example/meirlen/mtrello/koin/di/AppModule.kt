package com.example.meirlen.mtrello.koin.di

import com.example.meirlen.mtrello.rest.ApiService
import com.example.meirlen.mtrello.constants.Constant
import com.example.meirlen.mtrello.constants.Constant.BASE_URL
import com.example.meirlen.mtrello.koin.DetailViewModel
import com.example.meirlen.mtrello.koin.repository.WeatherRepository
import com.example.meirlen.mtrello.koin.repository.WeatherRepositoryImpl
import com.example.meirlen.mtrello.util.AuthInterceptor
import com.example.meirlen.mtrello.util.rx.ApplicationSchedulerProvider
import com.example.meirlen.mtrello.util.rx.SchedulerProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val appModule = module {
    factory {
        createOkHttpClient()
    }
    factory {
        createWebService<ApiService>(get(), BASE_URL)
    }

    module("repository") {

        factory {
            WeatherRepositoryImpl(get()) as WeatherRepository
        }

        module("viewModel") {
            viewModel {
                DetailViewModel(get(), get())
            }
        }

    }

}
val rxModule = module {

    single { ApplicationSchedulerProvider() as SchedulerProvider }
}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val okHttpBuilder = OkHttpClient.Builder()
            .connectTimeout(Constant.CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(Constant.READ_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(AuthInterceptor())
            .addInterceptor(httpLoggingInterceptor)
    return okHttpBuilder.build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}
