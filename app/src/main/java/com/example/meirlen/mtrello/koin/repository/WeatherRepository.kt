package com.example.meirlen.mtrello.koin.repository

import com.example.meirlen.mtrello.rest.ApiService
import com.example.meirlen.mtrello.model.Board
import io.reactivex.Single


interface WeatherRepository {
    fun getWeather(): Single<List<Board>>
}


class WeatherRepositoryImpl(private val mApiService: ApiService) : WeatherRepository {
    override fun getWeather(): Single<List<Board>> = mApiService.getBoards2()
}