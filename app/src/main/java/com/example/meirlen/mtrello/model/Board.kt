package com.example.meirlen.mtrello.model

import com.example.meirlen.mtrello.testing.OpenForTesting

@OpenForTesting
data class Board(val id: String, val name: String)