package com.example.meirlen.mtrello.model

data class News(
        val desc: String,
        val title: String,
        val type: Int,
        val url: String,
        val childNews:List<News>
)