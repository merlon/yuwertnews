package com.example.meirlen.mtrello.repository

import androidx.lifecycle.LiveData
import com.example.meirlen.mtrello.util.AppExecutors
import com.example.meirlen.mtrello.rest.ApiResponse
import com.example.meirlen.mtrello.rest.ApiService
import com.example.meirlen.mtrello.model.Column
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.util.NetworkBoundResource2
import com.example.meirlen.mtrello.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class BoardColumnsRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val mApiService: ApiService
) {

    fun getColumns(sBoardId:String?): LiveData<Resource<List<Column>>> {
        return object : NetworkBoundResource2<List<Column>, List<Column>>(appExecutors) {

            override fun createCall(): LiveData<ApiResponse<List<Column>>> {
                return mApiService.getColumns(sBoardId)
            }
        }.asLiveData()
    }
}
