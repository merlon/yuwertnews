package com.example.meirlen.mtrello.repository

import androidx.lifecycle.LiveData
import com.example.meirlen.mtrello.util.AppExecutors
import com.example.meirlen.mtrello.rest.ApiResponse
import com.example.meirlen.mtrello.rest.ApiService
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.util.NetworkBoundResource2
import com.example.meirlen.mtrello.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class BoardRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val mApiService: ApiService
) {

    fun getBoards(): LiveData<Resource<List<Board>>> {
        return object : NetworkBoundResource2<List<Board>, List<Board>>(appExecutors) {

            override fun createCall(): LiveData<ApiResponse<List<Board>>> {
                return mApiService.getBoards()
            }
        }.asLiveData()
    }
}
