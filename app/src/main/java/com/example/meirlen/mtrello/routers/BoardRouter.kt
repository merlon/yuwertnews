package com.example.meirlen.mtrello.routers

import android.content.Context
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.ui.activity.ColumnsActivity
import javax.inject.Inject
import javax.inject.Singleton

@OpenForTesting
@Singleton
class BoardRouter @Inject internal constructor() {

    fun showColumns(context: Context?, id: String) {
        context?.let { it.startActivity(ColumnsActivity.getStartIntent(it, id)) }
    }
}