package com.example.meirlen.mtrello.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.ui.fragments.ColumnFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class ColumnsActivity : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private var mFragment: ColumnFragment? = null

    private lateinit var sBoardId: String

    companion object {

        const val EXTRA_BOARD_ID = "extra_board_id"

        fun getStartIntent(context: Context, boardId: String): Intent {
            val intent = Intent(context, ColumnsActivity::class.java)
            intent.putExtra(EXTRA_BOARD_ID, boardId)
            return intent
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_columns)
        setupColumns()
    }

    private fun setupColumns() {
        sBoardId = intent.getStringExtra(EXTRA_BOARD_ID)
        mFragment = ColumnFragment.newInstance(sBoardId)
        mFragment?.let {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, it)
                    .commit()
        }
    }


    override fun supportFragmentInjector() = dispatchingAndroidInjector

}
