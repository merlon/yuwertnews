package com.example.meirlen.mtrello.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.model.News
import com.example.meirlen.mtrello.interfaces.ItemClickListener
import com.example.meirlen.mtrello.util.loadImage
import kotlinx.android.synthetic.main.child_news_item.view.*
import kotlinx.android.synthetic.main.news_item_one.view.*

class ResultAdapter(private val context: Context, private var listener: ItemClickListener<News>) : RecyclerView.Adapter<ResultAdapter.MovieViewHolder>() {

    private var mList: ArrayList<News> = ArrayList()

    private val TYPE_SALE = 1
    private val TYPE_POPULAR = 2


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {

        lateinit var view: View
        when (viewType) {
            TYPE_SALE -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.news_item_one, parent, false)
            }
            TYPE_POPULAR -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.news_item_one, parent, false)
            }
        }

        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val news = mList[position]
        holder.itemView.txtTitle!!.text = news.title
        holder.itemView.setOnClickListener { listener.onItemClick(news) }
        holder.itemView.imageView.loadImage(news.url)
        holder.itemView.linearLayoutItems.removeAllViews()
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        /*news.childNews.forEach {
            val child = inflater.inflate(R.layout.item_card, null)
            val textViewTitle = child.txtTitle as TextView
            textViewTitle.text = it.title
            holder.itemView.linearLayoutItems.addView(child)
        }*/

        repeat(8) {
            val child = inflater.inflate(R.layout.child_news_item, null)
            child.product_img.loadImage("https://3.bp.blogspot.com/-GeiTfBN9ln0/WTr-HUr8ADI/AAAAAAAAP1U/NrnJCCQ8JsQFB21lmPHeyTZcL2PssmMIgCLcB/s1600/stikhi-pro-osen.jpg")
            holder.itemView.linearLayoutItems.addView(child)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return mList[position].type
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun setData(movieList: ArrayList<News>) {
        mList = movieList
        notifyDataSetChanged()
    }

    fun clearAdapter() {
        mList.clear()
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val view = this.itemView
    }

}