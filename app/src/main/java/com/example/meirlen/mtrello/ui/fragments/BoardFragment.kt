package com.example.meirlen.mtrello.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.base.BaseFragment
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.interfaces.ItemClickListener
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.ui.adapter.BoardsAdapter
import com.example.meirlen.mtrello.viewmodel.BoardsViewModel
import com.example.meirlen.mtrello.vo.Resource
import com.example.meirlen.mtrello.routers.BoardRouter
import com.example.meirlen.mtrello.model.Error
import kotlinx.android.synthetic.main.board_list_fragment.*

import javax.inject.Inject


@OpenForTesting
class BoardFragment : BaseFragment<List<Board>, BoardsViewModel>(), ItemClickListener<Board> {

    companion object {
        fun newInstance(): BoardFragment {
            return BoardFragment()
        }
    }

    private lateinit var mAdapter: BoardsAdapter
    @Inject
    lateinit var router: BoardRouter

    override fun getContentView(): Int {
        return R.layout.board_list_fragment
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.boards.observe(this, Observer { listResource ->
            if (listResource != null) {
                handleResponse(listResource)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = BoardsAdapter(this)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter

    }

    override fun onResponse(response: Resource<List<Board>>) {
        if (response.data != null && response.data.isNotEmpty()) {
            mAdapter.setData(response.data as ArrayList<Board>)
            showContent(response.data)
        } else {
            val error = Error.NonCritical(resources.getString(R.string.error_no_results))
            showError(error)
        }
    }

    override fun onItemClick(dataObject: Board) {
        router.showColumns(context, dataObject.id)
        toast(dataObject.id)
    }


}