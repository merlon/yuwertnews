package com.example.meirlen.mtrello.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.base.BaseFragment
import com.example.meirlen.mtrello.model.Column
import com.example.meirlen.mtrello.interfaces.ItemClickListener
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.vo.Resource
import kotlinx.android.synthetic.main.board_list_fragment.*
import com.example.meirlen.mtrello.model.Error
import com.example.meirlen.mtrello.ui.adapter.ColumnAdapter
import com.example.meirlen.mtrello.viewmodel.ColumsViewModel

import javax.inject.Inject


@OpenForTesting
class ColumnFragment : BaseFragment<List<Column>, ColumsViewModel>(), ItemClickListener<Column> {

    companion object {
        private const val EXTRA_BOARD_ID = "ex_board_id"
        fun newInstance(board_id: String): ColumnFragment {
            val fragment = ColumnFragment()
            val args = Bundle()
            args.putString(EXTRA_BOARD_ID, board_id)
            fragment.arguments = args
            return fragment
        }
    }

    private val TAG = "ColumnFragment"
    private lateinit var mAdapter: ColumnAdapter
    private var sBoardId: String? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.columns(sBoardId).observe(this, Observer { listResource ->
            if (listResource != null) {
                handleResponse(listResource)
            }
        })
    }

    override fun getContentView(): Int {
        return R.layout.board_list_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            sBoardId = it.getString(EXTRA_BOARD_ID)
        }

        mAdapter = ColumnAdapter(context!!, this)
        mRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.adapter = mAdapter

    }

    override fun onResponse(response: Resource<List<Column>>) {
        if (response.data != null && response.data.isNotEmpty()) {
            mAdapter.setData(response.data as ArrayList<Column>)
            showContent(response.data)
        } else {
            val error = Error.NonCritical(resources.getString(R.string.error_no_results))
            showError(error)
        }
    }


    override fun onItemClick(dataObject: Column) {
        Log.d(TAG, "onResponse: ${dataObject.name}")
    }


}