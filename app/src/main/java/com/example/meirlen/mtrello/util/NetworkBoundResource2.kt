package com.example.meirlen.mtrello.util


import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.meirlen.mtrello.rest.ApiEmptyResponse
import com.example.meirlen.mtrello.rest.ApiErrorResponse
import com.example.meirlen.mtrello.rest.ApiResponse
import com.example.meirlen.mtrello.rest.ApiSuccessResponse
import com.example.meirlen.mtrello.vo.Resource

import org.json.JSONException
import org.json.JSONObject




/**
 * NetworkBoundResource only for network request , without Room
 **/
abstract class NetworkBoundResource2 <ResultType, RequestType> @MainThread
internal constructor(private val appExecutors: AppExecutors) {


    private val result = MediatorLiveData<Resource<RequestType>>()

    init {
        result.value = Resource.loading(null)
        // LiveData<ResultType> dbSource = loadFromDb();

        val apiResponse = createCall()
        // result.addSource(apiResponse, newData -> result.setValue(Resource.loading(newData.body)));
        result.addSource(apiResponse) { response ->

            when (response) {
                is ApiSuccessResponse -> {
                    appExecutors.mainThread().execute {
                        // reload from disk whatever we had
                        result.value = Resource.success(response.body)
                        asLiveData()
                    }
                }

                is ApiEmptyResponse -> {
                    appExecutors.mainThread().execute {
                        // reload from disk whatever we had
                        result.value = Resource.success(null)
                        asLiveData()
                    }
                }

                is ApiErrorResponse -> {
                    try {
                        val jObjError = JSONObject(response.errorMessage)
                        val message = jObjError.getString("message")
                        result.value = Resource.error(message, null);

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        result.value = Resource.error(response.errorMessage, null);

                    }

                    onFetchFailed()
                }

            }

        }

    }


    protected fun onFetchFailed() {}

    fun asLiveData(): LiveData<Resource<RequestType>> {
        return result
    }

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>

    }


