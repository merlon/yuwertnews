package com.example.meirlen.mtrello.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.repository.BoardRepository
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.vo.Resource
import javax.inject.Inject


@OpenForTesting
class BoardsViewModel @Inject constructor(trelloRepository: BoardRepository) : ViewModel() {

    val boards: LiveData<Resource<List<Board>>> = trelloRepository.getBoards()


}