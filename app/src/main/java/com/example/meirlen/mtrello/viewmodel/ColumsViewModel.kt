package com.example.meirlen.mtrello.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.meirlen.mtrello.model.Board
import com.example.meirlen.mtrello.model.Column
import com.example.meirlen.mtrello.repository.BoardColumnsRepository
import com.example.meirlen.mtrello.repository.BoardRepository
import com.example.meirlen.mtrello.testing.OpenForTesting
import com.example.meirlen.mtrello.vo.Resource
import javax.inject.Inject


@OpenForTesting
class ColumsViewModel @Inject constructor(val repository: BoardColumnsRepository) : ViewModel() {

    fun columns(sBoardId: String?): LiveData<Resource<List<Column>>> {
        return repository.getColumns(sBoardId)
    }
}